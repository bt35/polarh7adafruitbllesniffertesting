# PolarH7AdafruitBLLESnifferTesting

Testing PolarH7 heart rate monitor with Adafruit Bluetooth Friend, Wireshark, Tshark and Python

## Wireshark Data Capture 

This image shows when the PolarH7 was not connected and then when it connected or rather when a device subscribed to the heart rate service. When can see the device is not connected when the farthest right column shows ADV_IND, and then it is connected when we see ADV_NONCONN_IND.[1] I verfied this because it changed when I connected the HR Monitor app from BM Innovations GmbH, its a good app and it works. A lot of the Heart Rate apps just do not work. 

For this test process be sure to capture with wireshark or tshark and save it to a pcapng for post processing. 

![capture](/img/listentingtoconnected.png "WireShark Capture Showing Connecting")


Below is the tshark output. 

```
$ tshark -r heartRatetest.pcapng -Y 'btle.advertising_address == 00:22:d0:92:08:3a' -T fields -e 'frame.number' -e 'btle.advertising_address' -e 'btcommon.eir_ad.entry.data'
10      00:22:d0:92:08:3a       2b134845
40      00:22:d0:92:08:3a       2f134845
59      00:22:d0:92:08:3a       33134a46
73      00:22:d0:92:08:3a       3b134946
95      00:22:d0:92:08:3a       23134646
117     00:22:d0:92:08:3a       23134646
142     00:22:d0:92:08:3a       27134345
158     00:22:d0:92:08:3a       2b134345
180     00:22:d0:92:08:3a       2f134144
208     00:22:d0:92:08:3a       33134144
252     00:22:d0:92:08:3a       3b134144
274     00:22:d0:92:08:3a       3f134645
296     00:22:d0:92:08:3a       2f1b5845
321     00:22:d0:92:08:3a       2f134b46
344     00:22:d0:92:08:3a       33134b47
365     00:22:d0:92:08:3a       37134b47
407     00:22:d0:92:08:3a       2313504a
427     00:22:d0:92:08:3a       2313504a
453     00:22:d0:92:08:3a       2b13534b
476     00:22:d0:92:08:3a       2f13534c
497     00:22:d0:92:08:3a       3313534c
519     00:22:d0:92:08:3a       3713534d
539     00:22:d0:92:08:3a       3f13564e
565     00:22:d0:92:08:3a       3f13564e
587     00:22:d0:92:08:3a       2713564f
608     00:22:d0:92:08:3a       2b135650
625     00:22:d0:92:08:3a       33135952
645     00:22:d0:92:08:3a       33135952
664     00:22:d0:92:08:3a       3b135a53
687     00:22:d0:92:08:3a       3f135b54
736     00:22:d0:92:08:3a       2b135e56
779     00:22:d0:92:08:3a       33135e57
798     00:22:d0:92:08:3a       3b136059
818     00:22:d0:92:08:3a       3f13615a
847     00:22:d0:92:08:3a       2313615a
871     00:22:d0:92:08:3a       2713615b
893     00:22:d0:92:08:3a       2f13635c
922     00:22:d0:92:08:3a       3313645d
948     00:22:d0:92:08:3a       3b13655e
970     00:22:d0:92:08:3a       3f13675f
1000    00:22:d0:92:08:3a       27136961
1045    00:22:d0:92:08:3a       2f136a62
1069    00:22:d0:92:08:3a       33136a63
1123    00:22:d0:92:08:3a       3f136d66
1144    00:22:d0:92:08:3a       27136e67
1168    00:22:d0:92:08:3a       2b136e68
1190    00:22:d0:92:08:3a       33137069
1237    00:22:d0:92:08:3a       3f13736c
1258    00:22:d0:92:08:3a       2313736c
1282    00:22:d0:92:08:3a       2b136f6c
1304    00:22:d0:92:08:3a       2b136f6c
1324    00:22:d0:92:08:3a       33136d6c
1349    00:22:d0:92:08:3a       3713696b
1371    00:22:d0:92:08:3a       3f13696b
1418    00:22:d0:92:08:3a       2b13696b
1434    00:22:d0:92:08:3a       33135f68
1457    00:22:d0:92:08:3a       33135f68
1482    00:22:d0:92:08:3a       3b135966
1508    00:22:d0:92:08:3a       3f135162
1534    00:22:d0:92:08:3a       23135162
1562    00:22:d0:92:08:3a       27134b5f
1598    00:22:d0:92:08:3a       2b134b5f
1621    00:22:d0:92:08:3a       2f13495b
1641    00:22:d0:92:08:3a       37134e5b
1716    00:22:d0:92:08:3a       23134d57
1739    00:22:d0:92:08:3a       27134d57
1762    00:22:d0:92:08:3a       2b134955
1783    00:22:d0:92:08:3a       2f134955
1804    00:22:d0:92:08:3a       33134853
1827    00:22:d0:92:08:3a       37134853
1850    00:22:d0:92:08:3a       3b134852
1876    00:22:d0:92:08:3a       23134851
1899    00:22:d0:92:08:3a       23134851
1919    00:22:d0:92:08:3a       2b13474f
1942    00:22:d0:92:08:3a       2b13474f
1963    00:22:d0:92:08:3a       33134b4f
1985    00:22:d0:92:08:3a       37134b4f
2005    00:22:d0:92:08:3a       3f134e50
2033    00:22:d0:92:08:3a       3f134e50
```

## Python Hex Decoder. 

Take the output of Tshark to a file heartRateHex.txt. 

```
tshark -r heartRatetest.pcapng -Y 'btle.advertising_address == 00:22:d0:92:08:3a' -T fields -e 'btcommon.eir_ad.entry.data' > heartRateHex.txt
```

Here's how I decoded the hex from above. 

```
# Read the file in
filename= 'heartRateHex.txt'
# Open the file
theFileHandler=open(filename,"r")
# Read each line into a variable called lines
lines=theFileHandler.readlines()
# for each line in the lines variable do stuff
for x in lines:
# Split each two digits into its own variable. These are strings right now.
    a = x[0:2]
    b = x[3:5]
    c = x[6:8]
    d = x[9:10]
# Convert the strings into 16 bit integers. This converts the hex to decimal!
    e = int(a,16)
    f = int(b,16)
    g = int(c,16)
    h = int(a,16)
# Print the stuff so we can read it.
    print('{}, {}, {}, {}'.format(e, f, g, h))
# Close the file. Standard garbage collection stuff.
theFileHandler.close()
```

Here is the output of the above script. D is the heart rate number. This service specification is probably covered in many other github projects and also from Polar dev team. [2]

|a|b|c|d|
|--|--|--|--|
|43| 52| 69| 43  |
|47| 52| 69| 47  |
|51| 52| 70| 51  |
|59| 52| 70| 59  |
|35| 52| 70| 35  |
|35| 52| 70| 35  |
|39| 52| 69| 39  |
|43| 52| 69| 43  |
|47| 52| 68| 47  |
|51| 52| 68| 51  |
|59| 52| 68| 59  |
|63| 52| 69| 63  |
|47| 181| 69| 47 |
|47| 52| 70| 47  |
|51| 52| 71| 51  |
|55| 52| 71| 55  |
|35| 53| 74| 35  |
|35| 53| 74| 35  |
|43| 53| 75| 43  |
|47| 53| 76| 47  |
|51| 53| 76| 51  |
|55| 53| 77| 55  |
|63| 53| 78| 68  |
|63| 53| 78| 63  |
|39| 53| 79| 39  |
|43| 53| 80| 43  |
|51| 53| 82| 51  |
|51| 53| 82| 51  |
|59| 53| 83| 59  |
|63| 53| 84| 63  |
|43| 53| 86| 43  |
|51| 53| 87| 51  |
|59| 54| 89| 59  |
|63| 54| 90| 63  |
|35| 54| 90| 35  |
|39| 54| 91| 39  |
|47| 54| 92| 47  |
|51| 54| 93| 51  |
|59| 54| 94| 59  |
|63| 54| 95| 63  |
|39| 54| 97| 39  |
|47| 54| 98| 47  |
|51| 54| 99| 51  |
|63| 54| 102| 63 |
|39| 54| 103| 39 |
|43| 54| 104| 43 |
|51| 55| 105| 51 |
|63| 55| 108| 63 |
|35| 55| 108| 35 |
|43| 54| 108| 43 |
|43| 54| 108| 43 |
|51| 54| 108| 51 |
|55| 54| 107| 55 |
|63| 54| 107| 63 |
|43| 54| 107| 43 |
|51| 53| 104| 51 |
|51| 53| 104| 51 |
|59| 53| 102| 59 |
|63| 53| 98| 63  |
|35| 53| 98| 35  |
|39| 52| 95| 39  |
|43| 52| 95| 43  |
|47| 52| 91| 47  |
|55| 52| 91| 55  |
|35| 52| 87| 35  |
|39| 52| 87| 39  |
|43| 52| 85| 43  |
|47| 52| 85| 47  |
|51| 52| 83| 51  |
|55| 52| 83| 55  |
|59| 52| 82| 59  |
|35| 52| 81| 35  |
|35| 52| 81| 35  |
|43| 52| 79| 43  |
|43| 52| 79| 43  |
|51| 52| 79| 51  |
|55| 52| 79| 55  |
|63| 52| 80| 63  |
|63| 52| 80| 63  |

Here is the data from the HR Monitor App. If we compare column **c** above to column HR_BPM below. We have a pretty good idea that this decode is close enough to as accurate as a packet decode can be. 

|Sec  |  HR_bpm  |  DeltaRR_ms |
|-----|----------|-------------|
|0    |63    |0      | 
|1    |63    |0      |
|2    |63    |0      |
|3    |63    |0      |
|4    |64    |44     |
|5    |66    |42     |
|6    |67    |33     |
|7    |68    |19     |
|8    |69    |15     |
|9    |70    |17     |
|10   | 70   | 21    |
|11   | 70   | 32    |
|12   | 69   | 41    |
|13   | 68   | 39    |
|14   | 68   | 38    |
|15   | 69   | 55    |
|16   | 70   | 46    |
|17   | 71   | 41    |
|18   | 72   | 19    |
|19   | 75   | 27    |
|20   | 76   | 31    |
|21   | 78   | 25    |
|22   | 79   | 20    |
|23   | 82   | 24    |
|24   | 83   | 15    |
|25   | 85   | 14    |
|26   | 86   | 6     |
|27   | 89   | 8     |
|28   | 90   | 8     |
|29   | 92   | 9     |
|30   | 94   | 8     |
|31   | 97   | 10    |
|32   | 98   | 10    |
|33   | 100  |  8    |
|34   | 103  |  6    |
|35   | 105  |  6    |
|36   | 108  |  7    |
|37   | 108  |  15   |
|38   | 108  |  12   |
|39   | 107  |  30   |
|40   | 107  |  37   |
|41   | 104  |  34   |
|42   | 102  |  21   |
|43   | 98   | 28    |
|44   | 95   | 39    |
|45   | 91   | 75    |
|46   | 90   | 53    |
|47   | 87   | 70    |
|48   | 85   | 77    |
|49   | 82   | 48    |

### Graphical comparison of data. 

Data from decoded capture. This is a graph first table in this readme. 

![Heart Rate Data From Decoded Capture](/img/DataFromDecodedCapture.png "Heart Rate From the Decoding")

Data from heart rate app. This is a graph from the second table in this readme. 

![Heart Rate Data From App](/img/frommapp.png "Heart Rate From the App")



### Reference
[1] BTLE packet nomenclature. This guide shows the different BTLE packet types. 
https://www.novelbits.io/bluetooth-low-energy-sniffer-tutorial-advertisements/

Here is the bluetooth sniffer I have… but you shouldn’t buy this unless the device you are listening to is bluetooth low energy. 
[2] https://blog.alikhalil.tech/2014/11/polar-h7-bluetooth-le-heart-rate-sensor-on-ubuntu-14-04/


This is the sniffer that I used to listen for the BTLE stuff. 
[*] https://www.adafruit.com/product/2269
This is the adafruit guide for the BTLE Sniffer from above. 
[*] https://learn.adafruit.com/introducing-the-adafruit-bluefruit-le-sniffer

[*] Wireshark bluetooth low energy display filter language: 
https://www.wireshark.org/docs/dfref/b/btle.html


